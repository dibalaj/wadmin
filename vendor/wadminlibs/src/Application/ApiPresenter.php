<?
/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 *
 * WAdminLibs - Base API presenter class
 *
 * @author      Jakub Dibala
 * @package     WAdminCMS
 */
namespace WAdminLibs\Application;
use Nette;

class ApiPresenter extends Nette\Application\UI\Presenter
{
  /**
   * Presenter startup phase
   *
   * @return void
   */
  protected function startup()
  {
    // if user is not logged in, redirect to login page
    if (!$this->getUser()->isLoggedIn())
    {

    }

    parent::startup();
  }

}