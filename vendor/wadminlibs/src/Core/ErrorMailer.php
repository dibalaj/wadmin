<?
namespace WAdminLibs\Core;
use Nette;
use Tracy\Debugger;

class ErrorMailer
{
  public static function register()
  {
    $self = new static;
    Debugger::$onFatalError[] = [$self, 'handleError'];
  }


  public function handleError(\Exception $exc)
  {
    $msg = new Nette\Mail\Message();
    $msg->setFrom('ProfiSpol <apache@dibalaj.cz>')
      ->addTo('jakub.dibala@live.com')
      ->setSubject("test")
      ->setBody(print_r($exc, true));

    dump($msg->getHeaders());

    $mailer = new Nette\Mail\SendmailMailer;
    //$mailer->commandArgs = '-fprofispol@dibalaj.cz';
    $send = $mailer->send($msg);
  }
}