WAdmin CMS
=============

This project is a web based administration system, based on Nette Framework and Sencha ExtJS 4.

Bug tracker can be found on http://bug.dibalaj.cz/.

License
-------
- Nette: New BSD License or GPL 2.0 or 3.0 (http://nette.org/license)
- WAdmin CMS: GPL 3.0 License - see above
- images: https://www.flickr.com/photos/rubbermaid/4594830749

WAdmin CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

WAdmin CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
