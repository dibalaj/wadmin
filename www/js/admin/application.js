/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 *
 * Application definition
 *
 * @author      Jakub Dibala
 * @package     WAdminCMS
 */
Ext.application({
  name: 'WAdminCMS',
  enableQuickTips: true,
  appFolder: CFG_APP_DIR,
  paths: {
    'WAdminLibs': CFG_LIBS_DIR
  },

  /**
   * Application startup
   *
   * @returns {void}
   */
  launch: function() {
    var me = this;

    me.appManager   = Ext.create('WAdminLibs.AppManager');
    me.container = Ext.create('Ext.container.Viewport', {
      layout: 'border',
      items: [
        me.createTopInfoPanel(),
        me.createNavigationPanel(),
        me.createMainPanel(),
      ]
    });
  },


  /**
   * Creates top information panel
   *
   * @returns {Ext.container.Container}
   */
  createTopInfoPanel: function() {
    return Ext.create('Ext.container.Container', {
      region: 'north',
      height: 30,
      layout: 'column',
      cls: 'admin-top-info-panel',
      padding: 5,
      items: [
        {
          xtype: 'container',
          columnWidth: .70,
          cls: 'admin-app-owner',
          html: '<span>ProfiSpol, s.r.o. - administrace</span>'
        },
        {
          xtype: 'panel',
          columnWidth: .30,
          html: '@USER'
        }
      ]
    });
  },


  /**
   * Creates main navigation panel
   *
   * @returns {Ext.tree.Panel}
   */
  createNavigationPanel: function() {
    // tree store
    var store = Ext.create('Ext.data.TreeStore', {
      root: {
        children: [
          {
            text: _("Dashboard"),
            leaf: true,
            icon: '/icons/house.png',
            qtip: _("Main application dashboard")
          },
          {
            text: _("Web"),
            leaf: false,
            icon: "/icons/world.png",
            qtip: _("Web settings"),
            expanded: true,
            children: [
              {
                text: _("Pages"),
                leaf: true,
                icon: "/icons/page_white_world.png",
                qtip: _("Web pages management"),
                id: "WAdminCMS.Web.PagesList"
              }
            ]
          },
          {
            text: _("Company"),
            leaf: false,
            icon: "/icons/building.png",
            expanded: true,
            children: [
              {
                text: _("Job offers"),
                leaf: true,
                icon: "/icons/report.png",
                qtip: _("Job offers list"),
                id: "WAdminCMS.Company.JobsList"
              }
            ]
          },
          {
            text: _("System"),
            leaf: false,
            icon: '/icons/application.png',
            qtip: _("System settings"),
            children: [
              {
                text: _("Users"),
                icon: '/icons/user.png',
                leaf: true,
                qtip: _("Users management"),
                id: 'WAdminCMS.Users.List'
              },
              {
                text: "Uživatelské role",
                icon: "/icons/group_gear.png",
                leaf: true,
                qtip: "Správa uživatelských rolí",
                id: "WAdminCMS.Users.RolesList"
              },
              {
                text: _("Configurations"),
                icon: '/icons/application_form_edit.png',
                leaf: true,
                qtip: _("Configuration manager"),
                id: 'WAdminCMS.System.ConfigurationList'
              }
            ]
          }
        ]
      }
    });

    // panel
    var panel = Ext.create('Ext.tree.Panel', {
      title: _("Navigation"),
      width: 250,
      region: 'west',
      collapsible: true,
      split: true,
      rootVisible: false,
      margin: '0 0 5 5',
      store: store,
      listeners: {
        itemdblclick: function(pan, record, item, index) {
          // record is just a folder?
          if (record.data.leaf == false) {
            return;
          }

          // open tab with application
          WAdminCMS.getApplication().appManager.open(record.internalId, {
            title: record.data.text,
            icon: record.data.icon,
            closable: true
          });
        }
      }
    });

    return panel;
  },


  /**
   * Creates main content panel
   *
   * @returns {Ext.tab.Panel}
   */
  createMainPanel: function() {
    return Ext.create('Ext.tab.Panel', {
      region: 'center',
      plain: true,
      margin: '0 5 5 0',
      id: CMP_ID_APP_MAIN_CONTAINER,
      items: [
        {
          title: _("Dashboard"),
          bodyPadding: 10,
          html: '@TODO',
          icon: '/icons/house.png'
        }
      ]
    });
  }
});