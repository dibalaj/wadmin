/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 *
 * WAdminLibs - Application manager
 *
 * @author      Jakub Dibala
 * @package     WAdminCMS
 */
Ext.define('WAdminLibs.AppManager', {
  /**
   * Array of opened apps
   * @private
   */
  openedApps: [],


  /**
   * Open application and return its object
   *
   * @param {string } id Application ID
   * @param {object} params Application parameters
   * @returns {object}
   */
  open: function(id, params) {
    var me = this;

    // generate hash from parameters object
    var hash = id+JSON.stringify(params);

    console.log("APP_HASH:", hash);

    // if this application is opened, show it
    if (me.openedApps.hasOwnProperty(hash)) {
      var app = me.openedApps[hash];
      app.show();
      return app;
    }

    // try create app object
    try {

      var app = Ext.create(id, params);

    } catch (e) {

      Ext.Msg.show({
        title: _("ERROR"),
        msg: _("Application \"{0}\" cannot be opened!").format(id),
        icon: Ext.Msg.ERROR,
        buttons: Ext.Msg.OK
      });

      if (CFG_DEBUG)
        console.log(e);

      return null;
    }


    // common settings
    app.closable = true;
    app.title = _("Loading ...");

    // on application close, remove from our registry
    app.on("destroy", function() {
      delete me.openedApps[hash];
    });

    // save to registry
    me.openedApps[hash] = app;

    // add to main container
    var cmp = Ext.getCmp(CMP_ID_APP_MAIN_CONTAINER).add(app);

    // show, then setup app
    cmp.show();
    cmp.setup();

    // return it
    return cmp;
  }
});