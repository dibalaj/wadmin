/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 *
 * WAdminLibs - Sychronous AJAX
 *
 * @author      Jakub Dibala
 * @package     WAdminCMS
 */
Ext.define('WAdminLibs.SynchronousAjax', {
  extend: 'Ext.Base',

  /**
   * Is this request done?
   * @var {Boolean}
   */
  isDone: false,

  /**
   * Response object
   * @var {object}
   */
  response: null,

  id: 'ajax-test',


  /**
   * Call AJAX request
   *
   * @param {object} params
   * @return {object}
   */
  request: function(params) {
    var me = this;
    console.log(me);

    params.success = function(r) { me.onAjaxSuccess(r); };
    params.failure = function(r) { me.onAjaxFailure(r); };

    // send AJAX request
    Ext.Ajax.request(params);

    // now wait, until request is done
    var cnt = 0;
    while (cnt < 10) {
      // request is done?
      if (me.isDone == true) {
        console.log("["+me.self.getName()+"]: Request done!");
        break;
      }

      console.log("["+me.self.getName()+"]: Waiting ...");
      me.sleep(200);
      cnt++;
    }

    // return the response
    return me.response;
  },


  /**
   * Handler - on AJAX success
   */
  onAjaxSuccess: function(response) {
    console.log("OK", response);

    // mark this request as done
    this.isDone = true;

    // decode response
    this.response = Ext.decode(response.responseText);
  },


  /**
   * Handler - on AJAX failure
   */
  onAjaxFailure: function(response) {
    console.log("FAIL", response);

    // mark this request as done
    this.isDone = true;
  },


  sleep: function(ms) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
      if ((new Date().getTime() - start) > ms){
        break;
      }
    }
  }
});