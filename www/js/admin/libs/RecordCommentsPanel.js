/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 *
 * WAdminLibs - Record comments panel
 *
 * @author      Jakub Dibala
 * @package     WAdminCMS
 */
Ext.define('WAdminLibs.RecordCommentsPanel', {
  extend: "Ext.panel.Panel",
  icon: "/icons/comments.png",
  bodyStyle: 'background: #dfe9f6',
  layout: 'border',

  /**
   * Listeners config
   */
  listeners: {
    afterrender: function(me) {
      me.setup();
    }
  },

  /**
   * Constructor
   *
   * @param {String} table DB table name
   * @param {String} id DB primary key (id)
   */
  constructor: function(table, id) {
    var me = this;

    me._table = table;
    me._id = id;
    me.setTitle("Poznámky (0)");

    me.callParent();
  },


  /**
   * Setup function
   */
  setup: function() {
    var me = this;

    me.formPanel = me.createFormPanel();

    me.add(me.formPanel);
  },


  createFormPanel: function() {
    var me = this;

    return Ext.create('Ext.form.Panel', {
      title: 'Nová poznámka',
      layout: 'anchor',
      height: 80,
      region: 'north',
      margin: '10 0 10 0',
      defaults: {
        anchor: '100%',
      },
      items: [
        {
          xtype: 'textareafield',
          name: 'comment_text',
        }
      ]
    });
  }
});