/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 *
 * Locale: Czech
 *
 * @author      Jakub Dibala
 * @package     WAdminCMS
 */
LOCALES = {
  "Navigation": "Navigace",
  "Dashboard": "Nástěnka",
  "Main application dashboard": "Hlavní stránka přehledu aplikace",
  "System": "Systém",
  "System settings": "Nastavení systému",
  "Users": "Uživatelé",
  "Users management": "Správa uživatelských účtů",
  "Configurations": "Číselníky",
  "Configuration manager": "Správa číselníků",

  "Loading ...": "Načítám ...",
  "ERROR": "CHYBA",
  "Application \"{0}\" cannot be opened!": "Aplikaci \"{0}\" se nyní nepodařilo spustit!",

  "List of user accounts": "Seznam uživatelských účtů",
  "Loading records ...": "Načítám záznamy ...",
  "Reload records": "Znovu načíst záznamy",
  "Username": "Uživ. jméno",
  "Name": "Jméno",
  "Surname": "Příjmení",
  "E-mail": "E-mail",
  "New user account": "Nový uživ. účet",

  "Web": "Web",
  "Web settings": "Nastavení stránek",
  "Pages": "Stránky",
  "Web pages management": "Správa webových stránek",
  "Company": "Firma",
  "Job offers": "Nabídky práce",
  "Job offers list": "Seznam nabídek práce",

  "Delete key": "Smazat klíč",
  "Key": "Klíč",
  "Key is required field": "Klíč je povinná položka",
  "Value": "Hodnota",
  "Value is required field": "Hodnota je povinná položka",
  "Description": "Popis",
  "Description is required field": "Popis je povinná položka",
  "Save changes": "Uložit změny",
  "Information": "Informace",
  "Key was successfully updated": "Klíč byl úspěšně aktualizován",
  "Failed to update key data!": "Nepodařilo se aktualizovat záznam klíče!",
  "Key was successfully deleted": "Klíč byl úspěšně smazán",
  "Key delete failed!": "Smazání klíče selhalo!",
  "New configuration group": "Nová skupina konfigurace",
  "Group name": "Název skupiny",
  "Group name is required field": "Název skupiny je povinná položka",
  "Group description is required field": "Popis skupiny je povinná položka",
  "Create": "Vytvořit",
  "Group was successfully added": "Skupina byla úspěšně přidána",
  "Failed to create new group!": "vytvoření nové skupiny selhalo!",
  "New configuration key for: {0}": "Nový klíč konfigurace pro: {0}",
  "Create key": "Vytvořit klíč",
  "Key was successfully added": "Klíč byl úspěšně vytvořen",
  "Failed to create configuration key!": "Vytvoření konfiguračního klíče selhalo!",
  "Add new group": "Vytvořit novou skupinu",
  "Configurations": "Konfigurace",
  "Configuration groups list": "Seznam konfiguračních skupin",
  "New key": "Nový klíč",
  "Delete group": "Smazat skupinu",
  "Loading data ...": "Načítám data ...",
  "Config keys: {0}": "Konfigurační klíče: {0}",
  "Configuration keys: {0}": "Konfigurační klíče: {0}",
  "Group and its keys was successfully deleted": "Skupina a její klíče byly úspěšně smazány",
  "Group delete failed!": "Smazání skupiny selhalo!",


  "Add offer": "Přidat nabídku",
  "Job offers list": "Seznam prac. nabídek",
  "Offers listing": "Výpis nabídek",
  "Place": "Místo",
  "Reward": "Odměna",
  "Date": "Datum",


  "Web pages list": "Seznam web. stránek",
  "List of pages": "Seznam stránek",
  "Delete page": "Smazat stránku",
  "Web page: {0}": "Web. stránka: {0}",
  "All changes have been successfully saved": "Všechny změny byly úspěšně uloženy",
  "An error occured while saving changes!": "Při ukládání změn nastala chyba!",
  "An error occured while saving page content!": "Při ukládání obsahu stránky nastala chyba!",
  "Delete page?": "Smazat stránku?",
  "Do you really want to delete this web page?": "Opravdu si přejete smazat tuto webovou stránku?",
  "Page has been successfully deleted": "Stránka byla úspěšně smazána",
  "An error occured while deleting page!": "Při mazání stránky došlo k chybě!",
  "Failed to load page data!": "Selhalo načtení obsahu stránky!",
  "Settings": "Nastavení",
  "Content": "Obsah",
  "Web page settings": "Nastavení webové stránky",
  "Page name": "Název stránky",
  "Page name is required field!": "Název stránky je povinná položka!",
  "URL address": "URL adresa",
  "URL address is required field!": "URL adresa je povinná položka!",
  "Published?": "Publikováno?",


};


/**
 *
 *
 * @param string
 * @returns {*}
 * @private
 */
function _(string)
{
  if (LOCALES.hasOwnProperty(string)) {
    return LOCALES[string];
  }
  return string;
}

String.prototype.format = function() {
  var formatted = this;
  for (var i = 0; i < arguments.length; i++) {
    var regexp = new RegExp('\\{'+i+'\\}', 'gi');
    formatted = formatted.replace(regexp, arguments[i]);
  }
  return formatted;
};