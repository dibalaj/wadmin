/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 *
 * WAdminCMS - Configuration - item add window
 *
 * @author      Jakub Dibala
 * @package     WAdminCMS
 */
Ext.define('WAdminCMS.System.ConfigurationItemAddWindow', {
  extend: 'Ext.window.Window',
  icon: '/icons/application_key.png',
  modal: true,
  layout: 'fit',
  id: CMP_ID_APP_SYSTEM_CONFIGURATIONITEMADDWINDOW,


  /**
   * Constructor
   *
   * @param {object} group
   */
  constructor: function(group) {
    this.group = group;
    this.callParent();
  },


  /**
   * Setup function
   */
  setup: function() {
    var me = this;

    me.setTitle(_("New configuration key for: {0}").format(me.group.name));
    me.createForm();
  },


  /**
   * Creates form
   */
  createForm: function() {
    var me = this;

    var form = Ext.create('Ext.form.Panel', {
      layout: 'anchor',
      url: PROXY_URL_SYSTEM_CONFIGURATIONITEM_ADD.format(me.group.id),
      id: CMP_ID_APP_SYSTEM_CONFIGURATIONITEMADDWINDOW_FORM,
      border: false,
      defaults: {
        anchor: '100%',
        msgTarget: 'under'
      },
      width: 500,
      bodyPadding: 10,
      items: [
        {
          xtype: 'textfield',
          name: 'key',
          fieldLabel: _("Key"),
          allowBlank: false,
          blankText: _("Key is required field")
        },
        {
          xtype: 'textfield',
          name: 'value',
          fieldLabel: _("Value"),
          allowBlank: false,
          blankText: _("Value is required field")
        },
        {
          xtype: 'textareafield',
          name: 'description',
          fieldLabel: _("Description"),
          allowBlank: false,
          blankText: _("Description is required field")
        }
      ],
      buttons: [
        {
          text: _("Create key"),
          margin: '5 10 5 5',
          handler: me.onFormSubmitted
        }
      ]
    });

    me.add(form);
  },


  /**
   * Handler - on form submitted
   */
  onFormSubmitted: function() {
    // get form object
    var form = Ext.getCmp(CMP_ID_APP_SYSTEM_CONFIGURATIONITEMADDWINDOW_FORM).getForm();

    // is form valid?
    if (!form.isValid()) {
      return;
    }

    // submit form
    form.submit({

      success: function() {
        Ext.Msg.show({
          title: _("Information"),
          msg: _("Key was successfully added"),
          icon: Ext.Msg.INFO,
          buttons: Ext.Msg.OK
        });

        // reload items list store
        Ext.data.StoreManager.lookup(CMP_ID_STORE_SYSTEM_CONFIGURATIONITEMSLIST).reload();

        // close self
        Ext.getCmp(CMP_ID_APP_SYSTEM_CONFIGURATIONITEMADDWINDOW).close();
      },

      failure: function(form, action) {
        Ext.Msg.show({
          title: _("ERROR"),
          msg: _("Failed to create configuration key!"),
          icon: Ext.Msg.ERROR,
          buttons: Ext.Msg.OK
        });
      }
    });
  }
});