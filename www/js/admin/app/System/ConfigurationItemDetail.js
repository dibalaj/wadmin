/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 *
 * WAdminCMS - Configuration - item detail
 *
 * @author      Jakub Dibala
 * @package     WAdminCMS
 */
Ext.define('WAdminCMS.System.ConfigurationItemDetail', {
  extend: 'Ext.window.Window',
  icon: '/icons/application_key.png',
  modal: true,
  id: CMP_ID_APP_SYSTEM_CONFIGURATIONITEMDETAILWINDOW,
  layout: 'fit',
  tbar: [
    {
      text: _("Delete key"),
      icon: "/icons/cancel.png",
      handler: function() {
        Ext.getCmp(CMP_ID_APP_SYSTEM_CONFIGURATIONITEMDETAILWINDOW).keyDelete();
      }
    }
  ],


  /**
   * Constructor
   *
   * @param {object} item
   */
  constructor: function(item) {
    this.item = item;
    this.callParent();
  },


  /**
   * Setup function
   */
  setup: function() {
    var me = this;

    me.setTitle("Configuration key: {0}".format(me.item.key));
    me.createForm();
  },


  /**
   * Creates form
   */
  createForm: function() {
    var me = this;

    var form = Ext.create('Ext.form.Panel', {
      layout: 'anchor',
      url: PROXY_URL_SYSTEM_CONFIGURATIONITEM_UPDATE.format(me.item.id),
      id: CMP_ID_APP_SYSTEM_CONFIGURATIONITEMDETAILWINDOW_FORM,
      border: false,
      defaults: {
        anchor: '100%',
        msgTarget: 'under'
      },
      width: 500,
      bodyPadding: 10,
      items: [
        {
          xtype: 'textfield',
          name: 'key',
          fieldLabel: _("Key"),
          allowBlank: false,
          blankText: _("Key is required field"),
          value: me.item.key
        },
        {
          xtype: 'textfield',
          name: 'value',
          fieldLabel: _("Value"),
          allowBlank: false,
          blankText: _("Value is required field"),
          value: me.item.value
        },
        {
          xtype: 'textareafield',
          name: 'description',
          fieldLabel: _("Description"),
          allowBlank: false,
          blankText: _("Description is required field"),
          value: me.item.description
        }
      ],
      buttons: [
        {
          text: _("Save changes"),
          margin: '5 10 5 5',
          handler: me.onFormSubmitted
        }
      ]
    });

    me.add(form);
  },


  /**
   * Handler - on form submitted
   *
   * @returns {void}
   */
  onFormSubmitted: function() {
    // get form object
    var form = Ext.getCmp(CMP_ID_APP_SYSTEM_CONFIGURATIONITEMDETAILWINDOW_FORM).getForm();

    // is form valid?
    if (!form.isValid()) {
      return;
    }

    // submit form!
    form.submit({

      success: function() {
        Ext.Msg.show({
          title: _("Information"),
          msg: _("Key was successfully updated"),
          icon: Ext.Msg.INFO,
          buttons: Ext.Msg.OK
        });

        // reload groups list
        Ext.data.StoreManager.lookup(CMP_ID_STORE_SYSTEM_CONFIGURATIONITEMSLIST).reload();

        // close self
        Ext.getCmp(CMP_ID_APP_SYSTEM_CONFIGURATIONITEMDETAILWINDOW).close();
      },

      failure: function() {
        Ext.Msg.show({
          title: _("ERROR"),
          msg: _("Failed to update key data!"),
          icon: Ext.Msg.ERROR,
          buttons: Ext.Msg.OK
        });
      }
    });
  },


  /**
   * Deletes key
   */
  keyDelete: function() {
    var me = this;

    // send AJAX request to backend
    Ext.Ajax.request({
      url: PROXY_URL_SYSTEM_CONFIGURATIONITEM_DELETE.format(me.item.id),

      success: function() {
        Ext.Msg.show({
          title: _("Information"),
          msg: _("Key was successfully deleted"),
          icon: Ext.Msg.INFO,
          buttons: Ext.Msg.OK
        });

        // reload items list store
        Ext.data.StoreManager.lookup(CMP_ID_STORE_SYSTEM_CONFIGURATIONITEMSLIST).reload();

        // close self
        me.close();
      },

      failure: function() {
        Ext.Msg.show({
          title: _("ERROR"),
          msg: _("Key delete failed!"),
          icon: Ext.Msg.ERROR,
          buttons: Ext.Msg.OK
        });
      }
    });
  }
});
