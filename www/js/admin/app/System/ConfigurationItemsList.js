/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 *
 * WAdminCMS - Configuration - group items list
 *
 * @author      Jakub Dibala
 * @package     WAdminCMS
 */
Ext.define('WAdminCMS.System.ConfigurationItemsList', {
  extend: 'Ext.panel.Panel',
  icon: '/icons/application_view_list.png',
  bodyPadding: 10,
  id: CMP_ID_APP_SYSTEM_CONFIGURATION_GROUP_ITEMS_LIST,
  bodyStyle: 'background: #dfe9f6',
  layout: 'fit',


  /**
   * Constructor
   *
   * @param {object} group
   */
  constructor: function(group) {
    var me = this;

    me.group = group;
    me.tbar = [
      {
        text: _("New key"),
        icon: '/icons/add.png',
        handler: function() {
          WAdminCMS.getApplication().appManager.open('WAdminCMS.System.ConfigurationItemAddWindow', me.group).center();
        }
      },
      {
        text: _("Delete group"),
        icon: "/icons/cancel.png",
        handler: function() {
          Ext.getCmp(CMP_ID_APP_SYSTEM_CONFIGURATION_GROUP_ITEMS_LIST).onGroupDeleteButtonClicked();
        }
      }
    ];

    me.callParent();
  },


  /**
   * Setup function
   */
  setup: function() {
    var me = this;

    me.setLoading(_("Loading data ..."));
    me.setTitle(_("Config keys: {0}").format(me.group.name));
    me.createList();
  },


  /**
   * Creates items list
   */
  createList: function() {
    var me = this;

    // store
    var store = Ext.create('Ext.data.Store', {
      storeId: CMP_ID_STORE_SYSTEM_CONFIGURATIONITEMSLIST,
      autoLoad: true,
      fields: ['key', 'description', 'value'],
      proxy: {
        type: 'ajax',
        url: PROXY_URL_SYSTEM_CONFIGURATIONITEMSLIST.format(this.group.id),
        reader: {
          type: "json",
          root: "data"
        }
      }
    });

    // grid
    var grid = Ext.create('Ext.grid.Panel', {
      store: Ext.data.StoreManager.lookup(CMP_ID_STORE_SYSTEM_CONFIGURATIONITEMSLIST),
      title: _("Configuration keys: {0}".format(me.group.name)),
      viewConfig: {
        loadingText: _("Loading records ...")
      },
      tools: [
        {
          type: 'refresh',
          tooltip: _("Reload records"),
          callback: function(g) {
            g.store.reload();
          }
        }
      ],
      columns: [
        {
          text: _("Key"),
          dataIndex: 'key',
          flex: 2,
          renderer: function(val) {
            return "<b>{0}</b>".format(val);
          }
        },
        {
          text: _("Value"),
          dataIndex: 'value',
          flex: 5
        },
        {
          text: _("Description"),
          dataIndex: 'description',
          flex: 4
        }
      ],
      listeners: {
        itemdblclick: function(grid, record) {
          WAdminCMS.getApplication().appManager.open('WAdminCMS.System.ConfigurationItemDetail', record.data).center();
        }
      }
    });

    me.add(grid);
    me.setLoading(false);
  },


  /**
   * Handler - on group delete button clicked
   */
  onGroupDeleteButtonClicked: function() {
    var me = this;

    // send AJAX request to backend
    Ext.Ajax.request({
      url: PROXY_URL_SYSTEM_CONFIGURATIONGROUP_DELETE.format(me.group.id),

      success: function() {
        Ext.Msg.show({
          title: _("Information"),
          msg: _("Group and its keys was successfully deleted"),
          icon: Ext.Msg.INFO,
          buttons: Ext.Msg.OK
        });

        // reload items list store
        Ext.data.StoreManager.lookup(CMP_ID_STORE_SYSTEM_CONFIGURATIONGROUPSLIST).reload();

        // close self
        me.close();
      },

      failure: function() {
        Ext.Msg.show({
          title: _("ERROR"),
          msg: _("Group delete failed!"),
          icon: Ext.Msg.ERROR,
          buttons: Ext.Msg.OK
        });
      }
    });
  }
});