/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 *
 * WAdminCMS - Configuration - group add window
 *
 * @author      Jakub Dibala
 * @package     WAdminCMS
 */
Ext.define('WAdminCMS.System.ConfigurationGroupAddWindow', {
  extend: 'Ext.window.Window',
  icon: '/icons/application_form_add.png',
  modal: true,
  layout: 'fit',
  id: CMP_ID_APP_SYSTEM_CONFIGURATIONGROUPADDWINDOW,


  /**
   * Setup function
   */
  setup: function() {
    this.setTitle(_("New configuration group"));
    this.createForm();
  },


  /**
   * Creates form
   *
   * @private
   */
  createForm: function() {
    var me = this;

    var form = Ext.create('Ext.form.Panel', {
      layout: 'anchor',
      url: PROXY_URL_SYSTEM_CONFIGURATIONGROUPADD,
      id: CMP_ID_APP_SYSTEM_CONFIGURATIONGROUPADDWINDOW_FORM,
      border: false,
      defaults: {
        anchor: '100%',
        msgTarget: 'under'
      },
      width: 500,
      bodyPadding: 10,
      items: [
        {
          xtype: 'textfield',
          name: 'name',
          fieldLabel: _("Group name"),
          allowBlank: false,
          blankText: _("Group name is required field")
        },
        {
          xtype: 'textareafield',
          name: 'description',
          fieldLabel: _("Description"),
          allowBlank: false,
          blankText: _("Group description is required field")
        }
      ],
      buttons: [
        {
          text: _("Create"),
          margin: '5 10 5 5',
          handler: me.onFormSubmitted
        }
      ]
    });

    me.add(form);
  },


  /**
   * Handler - on form submitted
   *
   * @returns {void}
   */
  onFormSubmitted: function() {
    // get form object
    var form = Ext.getCmp(CMP_ID_APP_SYSTEM_CONFIGURATIONGROUPADDWINDOW_FORM).getForm();

    // is form valid?
    if (!form.isValid()) {
      return;
    }

    // submit form!
    form.submit({

      success: function() {
        Ext.Msg.show({
          title: _("Information"),
          msg: _("Group was successfully added"),
          icon: Ext.Msg.INFO,
          buttons: Ext.Msg.OK
        });

        // reload groups list
        Ext.data.StoreManager.lookup(CMP_ID_STORE_SYSTEM_CONFIGURATIONGROUPSLIST).reload();

        // close self
        Ext.getCmp(CMP_ID_APP_SYSTEM_CONFIGURATIONGROUPADDWINDOW).close();
      },

      failure: function() {
        Ext.Msg.show({
          title: _("ERROR"),
          msg: _("Failed to create new group!"),
          icon: Ext.Msg.ERROR,
          buttons: Ext.Msg.OK
        });
      }
    });
  }
});