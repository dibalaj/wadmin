/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 *
 * WAdminCMS - Configuration - groups listing
 *
 * @author      Jakub Dibala
 * @package     WAdminCMS
 */
Ext.define('WAdminCMS.System.ConfigurationList', {
  extend: 'Ext.panel.Panel',
  icon: '/icons/application_icon_edit.png',
  layout: 'fit',
  id: CMP_ID_APP_SYSTEM_CONFIGURATIONLIST,
  bodyPadding: 10,
  bodyStyle: 'background: #dfe9f6',
  tbar: [
    {
      text: _("Add new group"),
      icon: '/icons/add.png',
      handler: function() {
        WAdminCMS.getApplication().appManager.open('WAdminCMS.System.ConfigurationGroupAddWindow').center();
      }
    }
  ],


  /**
   * Setup function
   */
  setup: function() {
    this.setTitle(_("Configurations"));
    this.setLoading(_("Loading ..."));
    this.createList();
  },


  /**
   * Creates groups list
   */
  createList: function() {
    var me = this;

    // groups list store
    var store = Ext.create('Ext.data.Store', {
      storeId: CMP_ID_STORE_SYSTEM_CONFIGURATIONGROUPSLIST,
      autoLoad: true,
      fields: ['name', 'description'],
      proxy: {
        type: 'ajax',
        url: PROXY_URL_SYSTEM_CONFIGURATIONGROUPSLIST,
        reader: {
          type: "json",
          root: "data"
        }
      }
    });

    // grid panel
    var grid = Ext.create('Ext.grid.Panel', {
      store: Ext.data.StoreManager.lookup(CMP_ID_STORE_SYSTEM_CONFIGURATIONGROUPSLIST),
      title: _("Configuration groups list"),
      viewConfig: {
        loadingText: _("Loading records ...")
      },
      tools: [
        {
          type: 'refresh',
          tooltip: _("Reload records"),
          callback: function(g) {
            g.store.reload();
          }
        }
      ],
      columns: [
        {
          text: _("Group name"),
          dataIndex: 'name',
          renderer: function(val) {
            return '<b>'+val+'</b>';
          },
          flex: 2
        },
        {
          text: _("Description"),
          dataIndex: 'description',
          flex: 5
        }
      ],
      listeners: {
        itemdblclick: function(grid, record) {
          WAdminCMS.getApplication().appManager.open('WAdminCMS.System.ConfigurationItemsList', record.data);
        }
      }
    });

    me.add(grid);
    me.setLoading(false);
  }
});