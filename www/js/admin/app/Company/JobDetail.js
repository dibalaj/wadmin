/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 *
 * WAdminCMS - Company - job offer detail page
 *
 * @author      Jakub Dibala
 * @package     WAdminCMS
 */
Ext.define('WAdminCMS.Company.JobDetail', {
  extend: 'Ext.panel.Panel',
  icon: '/icons/report_edit.png',
  bodyStyle: 'background: #dfe9f6',
  bodyPadding: 10,
  layout: 'fit',


  /**
   * Constructor
   *
   * @param {Object} offer offer object
   */
  constructor: function(offer) {
    this.offer = offer;
    this.tooltip = "Detail pracovní nabídky - "+this.offer.title;
    this.tbar = this.createTbar();
    this.callParent();
  },


  createTbar: function() {
    var me = this;

    return [
      {
        text: 'Uložit změny',
        icon: '/icons/disk.png',
        handler: function() {
          me.onSaveChangesButtonClicked();
        }
      }
    ];
  },


  /**
   * Setup function
   */
  setup: function() {
    var me = this;
    me.setTitle("Pracovní nabídka: #{0}".format(me.offer.id));
    me.setLoading("Načítám data ...");
    me.requestOfferData();
  },


  /**
   * Requests offer data over API
   */
  requestOfferData: function() {
    var me = this;

    // on success
    var onSuccess = function(response) {
      // decode response
      me.offer = Ext.decode(response.responseText).data;

      // create GUI items
      me.createComponents();

      me.setLoading(false);
    };

    // on failure
    var onFailure = function(response) {
      Ext.Msg.show({
        title: _("ERROR"),
        msg: "Selhalo načtení záznamu pracovní nabídky!",
        icon: Ext.Msg.ERROR,
        buttons: Ext.Msg.OK
      });

      me.close();
    };

    // send AJAX request to server
    Ext.Ajax.request({
      url: PROXY_URL_COMPANY_JOB_DETAIL.format(me.offer.id),
      success: onSuccess,
      failure: onFailure
    });
  },


  /**
   * Creates GUI components
   */
  createComponents: function() {
    var me = this;

    // detail panel
    me.detailFormTabWrapper = Ext.create('Ext.panel.Panel', {
      title: _("Detail nabídky"),
      icon: '/icons/report_edit.png',
      bodyPadding: '10 0 0 0',
      bodyStyle: 'background: #dfe9f6',
      items: [
        me.createDetailFormPanel()
      ]
    });

    // candidates list wrapper
    me.candidatesListTabWrapper = Ext.create('Ext.panel.Panel', {
      title: _("Zájemci (0)"),
      icon: '/icons/user_gray.png',
      bodyPadding: '10 0 0 0',
      bodyStyle: 'background: #dfe9f6',
      layout: 'fit',
      items: [
        me.createCandidatesList()
      ]
    });

    me.commentsTabWrapper = Ext.create('WAdminLibs.RecordCommentsPanel', 'jobs2', me.offer.id);

    me.tabPanel = Ext.create('Ext.tab.Panel', {
      plain: true,
      cls: 'admin-inner-tabs',
      items: [
        me.detailFormTabWrapper,
        me.candidatesListTabWrapper,
        me.commentsTabWrapper
      ]
    });

    me.add(me.tabPanel);
  },


  /**
   * Creates detail form panel
   *
   * @returns {Ext.form.Panel}
   */
  createDetailFormPanel: function() {
    var me = this;

    me.formPanel = Ext.create('Ext.form.Panel', {
      title: 'Základní údaje prac. nabídky',
      icon: '/icons/report_edit.png',
      layout: 'anchor',
      width: 600,
      bodyPadding: 10,
      defaults: {
        anchor: '100%',
        msgTarget: 'under'
      },
      items: [
        {
          xtype: 'textfield',
          fieldLabel: 'Název nabídky',
          name: 'description',
          allowBlank: false,
          blankText: 'Název nabídky je povinná položka!',
          width: 500,
          value: me.offer.title
        },
        {
          xtype: 'textfield',
          fieldLabel: 'Odměna',
          name: 'reward',
          allowBlank: false,
          blankText: 'Odměna je povinná položka',
          width: 500,
          value: me.offer.reward
        },
        {
          xtype: 'container',
          width: 500,
          layout: 'hbox',
          margin: '0 0 5 0',
          items: [
            {
              xtype: 'datefield',
              fieldLabel: 'Datum',
              name: 'date_from',
              allowBlank: false,
              blankText: 'Termín - datum od je povinná položka',
              width: 290,
              format: 'd.m.Y',
              value: me.offer.date_from
            },
            {
              xtype: 'datefield',
              labelSeparator: '',
              fieldLabel: '&nbsp; -',
              labelWidth: 15,
              name: 'date_to',
              allowBlank: false,
              blankText: 'Termín - datum do je povinná položka',
              format: 'd.m.Y',
              flex: 1,
              value: me.offer.date_to
            },
          ]
        },
        {
          xtype: 'container',
          width: 500,
          layout: 'hbox',
          margin: '0 0 5 0',
          items: [
            {
              xtype: 'textfield',
              fieldLabel: 'Čas',
              name: 'time_from',
              allowBlank: true,
              width: 290,
              value: me.offer.time_from
            },
            {
              xtype: 'textfield',
              labelSeparator: '',
              fieldLabel: '&nbsp; -',
              labelWidth: 15,
              name: 'time_to',
              allowBlank: true,
              flex: 1,
              value: me.offer.time_to
            },
          ]
        },
        {
          xtype: 'textfield',
          fieldLabel: 'Adresa',
          name: 'place_address',
          allowBlank: false,
          blankText: 'Adresa je povinná položka!',
          width: 500,
          value: me.offer.place_address
        },
        {
          xtype: 'container',
          layout: 'hbox',
          margin: '0 0 5 0',
          width: 500,
          items: [
            {
              xtype: 'textfield',
              fieldLabel: 'Město',
              name: 'place_city',
              allowBlank: false,
              blankText: 'Město je povinná položka!',
              flex: 1,
              value: me.offer.place_city
            },
            {
              xtype: 'textfield',
              fieldLabel: ' &nbsp; PSČ',
              name: 'place_zip_code',
              allowBlank: false,
              blankText: 'PSČ je povinná položka!',
              width: 120,
              labelWidth: 40,
              value: me.offer.place_zip_code
            }
          ]
        },
        {
          xtype: 'textareafield',
          fieldLabel: 'Informace',
          name: 'info',
          width: 500,
          grow: true,
          value: me.offer.info
        },
        {
          xtype: 'textfield',
          name: 'author_name',
          fieldLabel: 'Kontaktní osoba',
          allowBlank: false,
          blankText: 'Kontaktní osoba je povinná položka!',
          width: 500,
          value: me.offer.author_name
        },
        {
          xtype: 'textfield',
          name: 'author_email',
          fieldLabel: 'Kontaktní e-mail',
          allowBlank: false,
          blankText: 'Kontaktní e-mail je povinná položka!',
          width: 500,
          value: me.offer.author_email
        },
        {
          xtype: 'textfield',
          name: 'author_phone',
          fieldLabel: 'Kontaktní tel.č.',
          allowBlank: false,
          blankText: 'Kontaktní tel.č. je povinná položka!',
          width: 500,
          value: me.offer.author_phone
        }
      ]
    });

    return me.formPanel;
  },


  /**
   *
   */
  createCandidatesList: function() {
    var me = this;

    var store = Ext.create('Ext.data.Store', {
      storeId: CMP_ID_STORE_COMPANY_JOB_CANDIDATES_LIST,
      fields: ['id', 'name', 'birth_date', 'email', 'phone', 'city', 'created_date'],
      proxy: {
        type: 'ajax',
        url: PROXY_URL_COMPANY_JOB_CANDIDATES_LIST.format(me.offer.id),
        reader: {
          type: "json",
          root: "data"
        }
      }
    });

    // load logins store and update tab title
    store.load({
      scope: me,
      callback: function(records) {
        me.candidatesListTabWrapper.setTitle("Zájemci ({0})".format(records.length));
      }
    });

    me.candidatesList = Ext.create('Ext.grid.Panel', {
      store: Ext.data.StoreManager.lookup(CMP_ID_STORE_COMPANY_JOB_CANDIDATES_LIST),
      viewConfig: {
        loadingText: _("Loading records ...")
      },
      title: "Seznam zájemců",
      tools: [
        {
          type: 'refresh',
          tooltip: _("Reload records"),
          callback: function(g) {
            g.store.reload();
          }
        }
      ],
      columns: [
        {
          text: 'ID',
          dataIndex: 'id',
          flex: 1,
          renderer: function(id) {
            return '#'+id;
          }
        },
        {
          text: "Jméno a příjmení",
          dataIndex: 'name',
          flex: 2
        },
        {
          text: "Datum naroz.",
          dataIndex: 'birth_date',
          flex: 2
        },
        {
          text: "E-mail",
          dataIndex: 'email',
          flex: 3
        },
        {
          text: "Telefon",
          dataIndex: "phone",
          flex: 2
        },
        {
          text: "Město",
          dataIndex: "city",
          flex: 2
        },
        {
          text: "Zaregistrováno",
          dataIndex: "created_date",
          flex: 2
        }
      ]
    });

    return me.candidatesList;
  }
});