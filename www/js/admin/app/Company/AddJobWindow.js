/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 *
 * WAdminCMS - Company - job offer add window
 *
 * @author      Jakub Dibala
 * @package     WAdminCMS
 */
Ext.define('WAdminCMS.Company.AddJobWindow', {
  extend: 'Ext.window.Window',
  icon: '/icons/report_add.png',
  layout: 'fit',


  /**
   * Setup function
   */
  setup: function() {
    var me = this;
    me.setTitle("Přidat pracovní nabídku");
    me.setLoading(_("Loading ..."));
    me.createForm();
    me.setLoading(false);
  },


  /**
   * Create form and add to page
   */
  createForm: function() {
    var me = this;

    me.form = Ext.create('Ext.form.Panel', {
      layout: 'anchor',
      url: PROXY_URL_COMPANY_JOB_CREATE,
      defaults: {
        anchor: '100%',
        msgTarget: 'under'
      },
      border: false,
      bodyPadding: 10,
      items: [
        {
          xtype: 'textfield',
          fieldLabel: 'Název nabídky',
          name: 'description',
          allowBlank: false,
          blankText: 'Název nabídky je povinná položka!',
          width: 500
        },
        {
          xtype: 'textfield',
          fieldLabel: 'Odměna',
          name: 'reward',
          allowBlank: false,
          blankText: 'Odměna je povinná položka',
          width: 500
        },
        {
          xtype: 'container',
          width: 500,
          layout: 'hbox',
          margin: '0 0 5 0',
          items: [
            {
              xtype: 'datefield',
              fieldLabel: 'Datum',
              name: 'date_from',
              allowBlank: false,
              blankText: 'Termín - datum od je povinná položka',
              width: 290,
              format: 'd.m.Y',
              value: new Date()
            },
            {
              xtype: 'datefield',
              labelSeparator: '',
              fieldLabel: '&nbsp; -',
              labelWidth: 15,
              name: 'date_to',
              allowBlank: false,
              blankText: 'Termín - datum do je povinná položka',
              format: 'd.m.Y',
              flex: 1,
              value: new Date()
            },
          ]
        },
        {
          xtype: 'container',
          width: 500,
          layout: 'hbox',
          margin: '0 0 5 0',
          items: [
            {
              xtype: 'textfield',
              fieldLabel: 'Čas',
              name: 'time_from',
              allowBlank: true,
              width: 290,
            },
            {
              xtype: 'textfield',
              labelSeparator: '',
              fieldLabel: '&nbsp; -',
              labelWidth: 15,
              name: 'time_to',
              allowBlank: true,
              flex: 1
            },
          ]
        },
        {
          xtype: 'textfield',
          fieldLabel: 'Adresa',
          name: 'place_address',
          allowBlank: false,
          blankText: 'Adresa je povinná položka!',
          width: 500
        },
        {
          xtype: 'container',
          layout: 'hbox',
          margin: '0 0 5 0',
          width: 500,
          items: [
            {
              xtype: 'textfield',
              fieldLabel: 'Město',
              name: 'place_city',
              allowBlank: false,
              blankText: 'Město je povinná položka!',
              flex: 1
            },
            {
              xtype: 'textfield',
              fieldLabel: ' &nbsp; PSČ',
              name: 'place_zip_code',
              allowBlank: false,
              blankText: 'PSČ je povinná položka!',
              width: 120,
              labelWidth: 40
            }
          ]
        },
        {
          xtype: 'textareafield',
          fieldLabel: 'Informace',
          name: 'info',
          width: 500
        },
        {
          xtype: 'textfield',
          name: 'author_name',
          fieldLabel: 'Kontaktní osoba',
          allowBlank: false,
          blankText: 'Kontaktní osoba je povinná položka!',
          width: 500
        },
        {
          xtype: 'textfield',
          name: 'author_email',
          fieldLabel: 'Kontaktní e-mail',
          allowBlank: false,
          blankText: 'Kontaktní e-mail je povinná položka!',
          width: 500
        },
        {
          xtype: 'textfield',
          name: 'author_phone',
          fieldLabel: 'Kontaktní tel.č.',
          allowBlank: false,
          blankText: 'Kontaktní tel.č. je povinná položka!',
          width: 500
        }
      ],
      buttons: [
        {
          text: 'Přidat nabídku',
          icon: '/icons/add.png',
          margin: '0 5 10 0',
          handler: function() {
            me.onFormSubmitted();
          }
        }
      ]
    });

    me.add(me.form);
  },


  /**
   * HANDLER - on form submit button clicked
   */
  onFormSubmitted: function() {
    var me = this;

    // get form object
    var form = this.form.getForm();

    // form is not valid?
    if (!form.isValid())
      return false;

    // submit form
    form.submit({
      success: function() {
        Ext.Msg.show({
          title: _("Information"),
          msg: "Pracovní nabídka byla úspěšně vytvořena",
          icon: Ext.Msg.INFO,
          buttons: Ext.Msg.OK
        });

        // reload users store
        Ext.data.StoreManager.lookup(CMP_ID_STORE_COMPANY_JOBS_LIST).reload();

        // close self
        me.close();
      },

      failure: function() {
        Ext.Msg.show({
          title: _("ERROR"),
          msg: "Selhalo vytvoření záznamu pracovní nabídky",
          icon: Ext.Msg.ERROR,
          buttons: Ext.Msg.OK
        });
      }
    });
  }
});