/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 *
 * WAdminCMS - Configuration - groups listing
 *
 * @author      Jakub Dibala
 * @package     WAdminCMS
 */
Ext.define('WAdminCMS.Company.JobsList', {
  extend: 'Ext.panel.Panel',
  icon: '/icons/report.png',
  layout: 'fit',
  bodyPadding: 10,
  bodyStyle: 'background: #dfe9f6',
  tbar: [
    {
      text: _("Add offer"),
      icon: '/icons/add.png',
      handler: function() {
        WAdminCMS.getApplication().appManager.open('WAdminCMS.Company.AddJobWindow').center();
      }
    }
  ],


  /**
   * Setup function
   */
  setup: function() {
    this.setTitle(_("Job offers list"));
    this.setLoading(_("Loading ..."));
    this.createList();
  },


  /**
   * Creates groups list
   */
  createList: function() {
    var me = this;

    // groups list store
    var store = Ext.create('Ext.data.Store', {
      storeId: CMP_ID_STORE_COMPANY_JOBS_LIST,
      autoLoad: true,
      fields: ['id', 'title', 'place_city', 'reward', 'date_from'],
      proxy: {
        type: 'ajax',
        url: PROXY_URL_COMPANY_JOBS_LIST,
        reader: {
          type: "json",
          root: "data"
        }
      },
      sorters: {
        property: 'date_from',
        direction: 'DESC'
      }
    });

    // grid panel
    var grid = Ext.create('Ext.grid.Panel', {
      store: Ext.data.StoreManager.lookup(CMP_ID_STORE_COMPANY_JOBS_LIST),
      title: _("Offers listing"),
      viewConfig: {
        loadingText: _("Loading records ...")
      },
      tools: [
        {
          type: 'refresh',
          tooltip: _("Reload records"),
          callback: function(g) {
            g.store.reload();
          }
        }
      ],
      columns: [
        {
          text: _("ID"),
          dataIndex: 'id',
          flex: 1,
          renderer: function(id) {
            return '#'+id;
          }
        },
        {
          text: _("Description"),
          dataIndex: 'title',
          flex: 2
        },
        {
          text: _("Place"),
          dataIndex: 'place_city',
          flex: 2
        },
        {
          text: _("Reward"),
          dataIndex: 'reward',
          flex: 2
        },
        {
          text: _("Date"),
          dataIndex: 'date_from',
          flex: 2
        }
      ],
      listeners: {
        itemdblclick: function(grid, record) {
          WAdminCMS.getApplication().appManager.open('WAdminCMS.Company.JobDetail', record.data);
        }
      }
    });

    me.add(grid);
    me.setLoading(false);
  }
});