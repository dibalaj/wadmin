/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 *
 * WAdminCMS - Users list
 *
 * @author      Jakub Dibala
 * @package     WAdminCMS
 */
Ext.define('WAdminCMS.Users.List', {
  extend: 'Ext.panel.Panel',
  layout: 'fit',
  bodyPadding: 10,
  bodyStyle: 'background: #dfe9f6',
  tbar: [
    {
      text: _("New user account"),
      icon: '/icons/user_add.png',
      handler: function() {
        WAdminCMS.getApplication().appManager.open("WAdminCMS.Users.AddWindow").center();
      }
    }
  ],


  /**
   * Setup function
   */
  setup: function() {
    this.setTitle(_("Users"));
    this.setLoading(_("Loading ..."));
    this.createList();
  },


  /**
   * Creates users list
   *
   * @private
   */
  createList: function() {
    var me = this;

    // create store
    var store = Ext.create('Ext.data.Store', {
      storeId: CMP_ID_STORE_USERS_LIST,
      autoLoad: true,
      fields: ['name', 'surname', 'email', 'username'],
      proxy: {
        type: 'ajax',
        url: PROXY_URL_USERS_LIST,
        reader: {
          type: "json",
          root: "data"
        }
      }
    });

    // create grid
    var grid = Ext.create('Ext.grid.Panel', {
      store: Ext.data.StoreManager.lookup(CMP_ID_STORE_USERS_LIST),
      title: _("List of user accounts"),
      viewConfig: {
        loadingText: _("Loading records ...")
      },
      tools: [
        {
          type: 'refresh',
          tooltip: _("Reload records"),
          callback: function(g) {
            g.store.reload();
          }
        }
      ],
      columns: [
        {
          text: _("Username"),
          dataIndex: 'username',
          flex: 2
        },
        {
          text: _("Name"),
          dataIndex: 'name',
          flex: 3
        },
        {
          text: _("Surname"),
          dataIndex: 'surname',
          flex: 3
        },
        {
          text: _("E-mail"),
          dataIndex: 'email',
          flex: 3
        }
      ],
      listeners: {
        itemdblclick: function(grid, record) {
          WAdminCMS.getApplication().appManager.open('WAdminCMS.Users.Detail', record.data.id);
        }
      }
    });

    me.add(grid);
    me.setLoading(false);
  }
});