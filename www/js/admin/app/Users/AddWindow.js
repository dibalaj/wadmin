/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 *
 * WAdminCMS - Users list
 *
 * @author      Jakub Dibala
 * @package     WAdminCMS
 */
Ext.define('WAdminCMS.Users.AddWindow', {
  extend: 'Ext.window.Window',
  icon: '/icons/user_add.png',
  id: CMP_ID_APP_USERS_ADDWINDOW,

  /**
   * Setup function
   */
  setup: function() {
    this.setTitle(_("New user account"));
    this.createForm();
  },


  /**
   * Creates form
   */
  createForm: function() {
    var me = this;

    var form = Ext.create('Ext.form.Panel', {
      layout: 'anchor',
      url: PROXY_URL_USER_ACCOUNT_CREATE,
      id: CMP_ID_APP_USERS_ADDWINDOW_FORM,
      border: false,
      defaults: {
        anchor: '100%',
        msgTarget: 'under'
      },
      width: 400,
      bodyPadding: 10,
      items: [
        {
          xtype: 'textfield',
          name: 'username',
          fieldLabel: _("Username"),
          allowBlank: false,
          blankText: _("Username is required field")
        },
        {
          xtype: 'textfield',
          name: 'name',
          fieldLabel: _("Name"),
          allowBlank: false,
          blankText: _("Name is required field")
        },
        {
          xtype: 'textfield',
          name: 'surname',
          fieldLabel: _("Surname"),
          allowBlank: false,
          blankText: _("Surname is required field")
        },
        {
          xtype: 'textfield',
          name: 'email',
          fieldLabel: _("E-mail"),
          allowBlank: false,
          vtype: 'email',
          vtypeText: _("Please, enter valid e-mail address"),
          blankText: _("E-mail is required field")
        }
      ],
      buttons: [
        {
          text: 'Vytvořit',
          margin: '5 10 5 5',
          handler: me.onFormSubmit
        }
      ]
    });

    me.add(form);
  },


  /**
   * Handler - on form submitted
   */
  onFormSubmit: function() {
    // load form
    var form = Ext.getCmp(CMP_ID_APP_USERS_ADDWINDOW_FORM).getForm();

    // form is not valid?
    if (!form.isValid()) {
      return;
    }

    // submit!
    form.submit({
      success: function(form, action) {

        Ext.Msg.show({
          title: _("Information"),
          msg: _("User account has been successfully created"),
          icon: Ext.Msg.INFO,
          buttons: Ext.Msg.OK
        });

        // reload users store
        Ext.data.StoreManager.lookup(CMP_ID_STORE_USERS_LIST).reload();

        // close self
        Ext.getCmp(CMP_ID_APP_USERS_ADDWINDOW).close();
      },
      failure: function(form, action) {
        Ext.Msg.show({
          title: _("ERROR"),
          msg: _("Failed to create user account!"),
          icon: Ext.Msg.ERROR,
          buttons: Ext.Msg.OK
        });
      }
    });
  }
});