/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 *
 * WAdminCMS - User detail
 *
 * @author      Jakub Dibala
 * @package     WAdminCMS
 */
Ext.define('WAdminCMS.Users.Detail', {
  extend: 'Ext.panel.Panel',
  icon: '/icons/user_edit.png',
  bodyStyle: 'background: #dfe9f6',
  id: CMP_ID_APP_USERS_DETAIL,
  bodyPadding: 10,
  layout: 'fit',
  tbar: [
    {
      text: _("Save changes"),
      icon: '/icons/disk.png',
      handler: function() {
        Ext.getCmp(CMP_ID_APP_USERS_DETAIL).onSaveChanges();
      }
    },
    {
      text: _("Delete user"),
      icon: "/icons/cancel.png",
      handler: function() {
        Ext.getCmp(CMP_ID_APP_USERS_DETAIL).onDeleteButton();
      }
    }
  ],


  /**
   * Constructor
   *
   * @param {Number} userId
   */
  constructor: function(userId) {
    this.userId = userId;
    this.callParent();
  },


  /**
   * Setup function
   */
  setup: function() {
    this.setLoading(_("Loading user data ..."));
    this.requestData();
  },


  /**
   * Handler - on click "save changes" button
   */
  onSaveChanges: function() {
    var me = this;

    // load form object
    var form = Ext.getCmp(CMP_ID_APP_USERS_DETAIL_FORM).getForm();

    // is form valid?
    if (!form.isValid()) {
      return;
    }

    var msgFn = function(title, msg) {
      Ext.Msg.show({
        title: title,
        msg: msg,
        icon: Ext.Msg.INFO,
        buttons: Ext.Msg.OK
      });
    };

    // submit form!
    form.submit({
      success: function() {
        var values = form.getValues();
        me.setTitle(_("User: {0} {1}").format(values.name, values.surname));
        msgFn(_("Information"), _("All changes have been successfully saved"));
      },

      failure: function() {
        msgFn(_("ERROR"), _("Failed to save changes!"));
      }
    });
  },


  /**
   * Handler - on delete button clicked
   */
  onDeleteButton: function() {
    var me = this;

    // send AJAX request to backend
    Ext.Ajax.request({
      url: PROXY_URL_USER_ACCOUNT_DELETE.format(me.user.id),

      success: function() {
        Ext.Msg.show({
          title: _("Information"),
          msg: _("User was successfully deleted"),
          icon: Ext.Msg.INFO,
          buttons: Ext.Msg.OK
        });

        // reload users list store
        Ext.data.StoreManager.lookup(CMP_ID_STORE_USERS_LIST).reload();

        // close self
        me.close();
      },

      failure: function() {
        Ext.Msg.show({
          title: _("ERROR"),
          msg: _("User delete failed!"),
          icon: Ext.Msg.ERROR,
          buttons: Ext.Msg.OK
        });
      }
    });

  },


  /**
   * Requests user data load
   */
  requestData: function() {
    var me = this;

    var failureFn = function() {
      Ext.Msg.show({
        title: _("ERROR"),
        msg: _("Failed to load user data!"),
        icon: Ext.Msg.ERROR,
        buttons: Ext.Msg.OK
      });

      me.close();
    };

    // send AJAX request to load user data
    Ext.Ajax.request({
      url: PROXY_URL_USER_ACCOUNT_GET.format(this.userId),
      success: function(response) {
        try {
          var responseData = Ext.decode(response.responseText);
          if (!responseData.hasOwnProperty("data")) {
            failureFn();
            return;
          }

          me.user = responseData.data;
        } catch (e) {
          if (CFG_DEBUG)
            console.log(e, response);
        }

        me.setTitle(_("User: {0} {1}").format(me.user.name, me.user.surname));
        me.createItems();
        me.setLoading(false);
      },

      failure: function(response) {
        failureFn();
        return;
      }
    });
  },


  /**
   * Creates app components
   */
  createItems: function() {
    var me = this;

    var formPanel = Ext.create('Ext.form.Panel', {
      title: _("User data"),
      id: CMP_ID_APP_USERS_DETAIL_FORM,
      bodyPadding: 10,
      url: PROXY_URL_USER_ACCOUNT_UPDATE.format(me.user.id),
      width: 400,
      layout: 'anchor',
      defaults: {
        anchor: '100%',
        msgTarget: 'under'
      },
      items: [
        {
          xtype: 'textfield',
          name: 'name',
          allowBlank: false,
          blankText: _("Name is required field"),
          fieldLabel: 'Name',
          value: me.user.name
        },
        {
          xtype: 'textfield',
          name: 'surname',
          allowBlank: false,
          blankText: _("Surname is required field"),
          fieldLabel: 'Surname',
          value: me.user.surname
        },
        {
          xtype: 'textfield',
          name: 'email',
          allowBlank: false,
          blankText: _("E-mail is required field"),
          vtype: 'email',
          vtypeText: _("Please, enter valid e-mail address"),
          fieldLabel: _("E-mail"),
          value: me.user.email
        },
        {
          xtype: 'textfield',
          name: 'username',
          allowBlank: false,
          blankText: _("Username is required field"),
          fieldLabel: _("Username"),
          value: me.user.username
        }
      ]
    });

    var loginsStore = Ext.create('Ext.data.Store', {
      storeId: CMP_ID_STORE_USERS_LOGINS_LIST,
      fields: ['id', 'ip_addr', 'ip_host', 'created_date'],
      proxy: {
        type: 'ajax',
        url: PROXY_URL_USER_ACCOUNT_LOGINS_LIST.format(me.user.id),
        reader: {
          type: "json",
          root: "data"
        }
      }
    });

    // load logins store and update tab title
    loginsStore.load({
      scope: me,
      callback: function(records) {
        Ext.getCmp(CMP_ID_APP_USERS_LOGINS_LIST_TAB).setTitle(_("Logins ({0})").format(records.length));
      }
    });

    var loginsGrid = Ext.create('Ext.grid.Panel', {
      store: Ext.data.StoreManager.lookup(CMP_ID_STORE_USERS_LOGINS_LIST),
      viewConfig: {
        loadingText: _("Loading records ...")
      },
      title: _("User logins list"),
      tools: [
        {
          type: 'refresh',
          tooltip: _("Reload records"),
          callback: function(g) {
            g.store.reload();
          }
        }
      ],
      columns: [
        {
          text: 'ID',
          dataIndex: 'id',
          flex: 1
        },
        {
          text: _("Created date"),
          dataIndex: 'created_date',
          flex: 2
        },
        {
          text: _("IP address"),
          dataIndex: 'ip_addr',
          flex: 2
        },
        {
          text: _("IP hostname"),
          dataIndex: 'ip_host',
          flex: 3
        }
      ]
    });

    var tabs = Ext.create('Ext.tab.Panel', {
      plain: true,
      cls: 'admin-inner-tabs',
      items: [
        {
          title: _("Detail"),
          icon: '/icons/application.png',
          bodyPadding: '10 0 0 0',
          bodyStyle: 'background: #dfe9f6',
          items: [
            formPanel
          ]
        },
        {
          title: _("Logins"),
          id: CMP_ID_APP_USERS_LOGINS_LIST_TAB,
          icon: '/icons/key_go.png',
          bodyStyle: 'background: #dfe9f6',
          bodyPadding: '10 0 0 0',
          layout: 'fit',
          items: [
            loginsGrid
          ]
        }
      ]
    });

    // add tab component to application
    me.add(tabs);
  }
});