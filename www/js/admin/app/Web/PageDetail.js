/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 *
 * WAdminCMS - Web - page detail
 *
 * @author      Jakub Dibala
 * @package     WAdminCMS
 */
Ext.define('WAdminCMS.Web.PageDetail', {
  extend: 'Ext.panel.Panel',
  icon: '/icons/page_white_wrench.png',
  layout: 'fit',
  bodyPadding: 10,
  bodyStyle: 'background: #dfe9f6',


  /**
   * Constructor
   *
   * @param {object} page page object
   */
  constructor: function(page) {
    var me = this;

    me.page = page;
    me.tbar = [
      {
        text: _("Save changes"),
        icon: '/icons/disk.png',
        handler: function() {
          me.onSaveChangesButtonClicked();
        }
      },
      {
        text: _("Delete page"),
        icon: '/icons/cancel.png',
        handler: function() {
          me.onDeletePageButtonClicked();
        }
      }
    ];

    me.callParent();
  },


  /**
   * Setup function
   */
  setup: function() {
    var me = this;
    me.setTitle(_("Web page: {0}").format(me.page.title));
    me.setLoading(_("Loading record ..."));
    me.apiRequestPageData();
  },


  /**
   * Handler - on "save changes" button clicked
   */
  onSaveChangesButtonClicked: function() {
    var me = this;

    // get form object and its values
    var form = this.detailForm.getForm();
    var values = form.getValues();

    // form is not valid?
    if (!form.isValid()) {
      return;
    }

    // submit form
    form.submit({
      success: function() {
        // set new title
        me.setTitle(_("Web page: {0}").format(values.title));

        // reload pages store
        Ext.data.StoreManager.lookup(CMP_ID_STORE_WEB_PAGES_LIST).reload();

        // show info msg
        Ext.Msg.show({
          title: _("Information"),
          msg: _("All changes have been successfully saved"),
          icon: Ext.Msg.INFO,
          buttons: Ext.Msg.OK
        });
      },
      failure: function() {
        Ext.Msg.show({
          title: _("ERROR"),
          msg: _("An error occured while saving changes!"),
          icon: Ext.Msg.ERROR,
          buttons: Ext.Msg.OK
        });
      }
    });

    // page content
    var pageContent = me.contentForm.getValue();

    // update page content
    Ext.Ajax.request({
      url: PROXY_URL_WEB_PAGE_UPDATE_CONTENT.format(me.page.id),
      params: {
        content: pageContent
      },
      failure: function() {
        Ext.Msg.show({
          title: _("ERROR"),
          msg: _("An error occured while saving page content!"),
          icon: Ext.Msg.ERROR,
          buttons: Ext.Msg.OK
        });
      }
    });
  },


  /**
   * Handler - on "delete page" button clicked
   */
  onDeletePageButtonClicked: function() {
    var me = this;

    Ext.Msg.confirm(_("Delete page?"), _("Do you really want to delete this web page?"), function(ans) {
      // no|cancel?
      if (ans != "yes") return;

      // call page delete over API
      me.apiDeletePage();
    });
  },


  /**
   * Send API request to delete this page
   */
  apiDeletePage: function() {
    var me = this;

    // send API request
    Ext.Ajax.request({
      url: PROXY_URL_WEB_PAGE_DELETE.format(me.page.id),
      success: function() {
        Ext.Msg.show({
          title: _("Information"),
          msg: _("Page has been successfully deleted"),
          icon: Ext.Msg.INFO,
          buttons: Ext.Msg.OK
        });

        // reload pages store
        Ext.data.StoreManager.lookup(CMP_ID_STORE_WEB_PAGES_LIST).reload();

        // close self
        me.close();
      },
      failure: function() {
        Ext.Msg.show({
          title: _("ERROR"),
          msg: _("An error occured while deleting page!"),
          icon: Ext.Msg.ERROR,
          buttons: Ext.Msg.OK
        });
      }
    });
  },


  /**
   * Requests page data over AJAX
   */
  apiRequestPageData: function() {
    var me = this;

    // send request
    Ext.Ajax.request({
      url: PROXY_URL_WEB_PAGE_GET.format(me.page.id),

      success: function(response) {
        me.page = Ext.decode(response.responseText).data;
        me.createItems();
        me.setLoading(false);
      },

      failure: function() {
        Ext.Msg.show({
          title: _("ERROR"),
          msg: _("Failed to load page data!"),
          icon: Ext.Msg.ERROR,
          buttons: Ext.Msg.OK
        });

        me.close();
      }
    });

  },


  /**
   * Creates page components
   */
  createItems: function() {
    var me = this;

    // create form
    me.detailForm = me.createDetailForm();

    // create content form
    var contentForm = me.createContentForm();

    // create tab panel
    var tabPanel = Ext.create('Ext.tab.Panel', {
      plain: true,
      cls: 'admin-inner-tabs',
      items: [
        {
          title: _("Settings"),
          bodyStyle: 'background: #dfe9f6',
          icon: '/icons/wrench.png',
          items: [
            me.detailForm
          ]
        },
        {
          title: _("Content"),
          bodyStyle: 'background: #dfe9f6',
          icon: '/icons/page_white_edit.png',
          layout: 'fit',
          items: [
            contentForm
          ]
        }
      ]
    });

    // add tab panel to container
    me.add(tabPanel);
  },


  /**
   * Creates content form
   */
  createContentForm: function() {
    var me = this;

    me.contentForm = Ext.create('Ext.form.HtmlEditor', {
      title: _("Web page settings"),
      margin: '10 0 0 0',
      width: 500,
      value: me.page.content,
      border: false
    });

    return me.contentForm;
  },


  /**
   * Creates form panel and returns its object
   *
   * @returns {Ext.form.Panel}
   */
  createDetailForm: function() {
    var me = this;

    return Ext.create('Ext.form.Panel', {
      title: _("Web page settings"),
      bodyPadding: 10,
      margin: '10 0 0 0',
      url: PROXY_URL_WEB_PAGE_UPDATE.format(me.page.id),
      width: 500,
      layout: 'anchor',
      defaults: {
        anchor: '100%',
        msgTarget: 'under'
      },
      items: [
        {
          xtype: 'textfield',
          fieldLabel: _("Page name"),
          value: me.page.title,
          name: 'title',
          allowBlank: false,
          blankText: _("Page name is required field!")
        },
        {
          xtype: 'textfield',
          fieldLabel: _("URL address"),
          value: me.page.url,
          name: 'url',
          allowBlank: false,
          blankText: _("URL address is required field!")
        },
        {
          xtype: 'checkboxfield',
          fieldLabel: _("Published?"),
          checked: true,
          name: 'published'
        }
      ]
    });
  }
});