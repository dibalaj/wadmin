/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 *
 * WAdminCMS - Web - pages list
 *
 * @author      Jakub Dibala
 * @package     WAdminCMS
 */
Ext.define('WAdminCMS.Web.PagesList', {
  extend: 'Ext.panel.Panel',
  icon: "/icons/page_white_world.png",
  layout: 'fit',
  bodyPadding: 10,
  bodyStyle: 'background: #dfe9f6',


  /**
   * Setup function
   */
  setup: function() {
    this.setTitle(_("Web pages list"));
    this.createList();
  },


  /**
   * Request pages list from backend
   */
  createList: function() {
    var me = this;

    Ext.define('Page', {
      extend: 'Ext.data.Model',
      fields: [
        {name: 'title',   type: 'string'},
        {name: 'url',     type: 'string'},
      ]
    });

    // create store
    var store = Ext.create('Ext.data.Store', {
      storeId: CMP_ID_STORE_WEB_PAGES_LIST,
      autoLoad: true,
      model: 'Page',
      proxy: {
        type: 'ajax',
        url: PROXY_URL_WEB_PAGES_LIST,
        reader: {
          type: "json",
          root: "data"
        }
      }
    });

    // create grid
    var grid = Ext.create('Ext.grid.Panel', {
      store: Ext.data.StoreManager.lookup(CMP_ID_STORE_WEB_PAGES_LIST),
      title: _("List of pages"),
      viewConfig: {
        loadingText: _("Loading records ...")
      },
      tools: [
        {
          type: 'refresh',
          tooltip: _("Reload records"),
          callback: function(g) {
            g.store.reload();
          }
        }
      ],
      columns: [
        {
          text: _("Name"),
          dataIndex: 'title',
          flex: 3
        },
        {
          text: _("URL"),
          dataIndex: 'url',
          renderer: function (val) {
            return '/'+val;
          },
          flex: 2
        },

      ],
      listeners: {
        itemdblclick: function(grid, record) {
          WAdminCMS.getApplication().appManager.open('WAdminCMS.Web.PageDetail', record.data);
        }
      }
    });

    me.add(grid);
    me.setLoading(false);
  }
});