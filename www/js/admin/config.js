/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 *
 * Application definition
 *
 * @author      Jakub Dibala
 * @package     WAdminCMS
 */

/**
 * Application directory (absolute)
 * @type {string}
 */
CFG_APP_DIR = "/js/admin/app";

/**
 * Libraries directory (absolute)
 * @type {string}
 */
CFG_LIBS_DIR = "/js/admin/libs";

/**
 * Default language
 * @type {string}
 */
CFG_LANG_DEFAULT = "en";

/**
 * Debug mode
 * @type {boolean}
 */
CFG_DEBUG = true;

/**
 * Application components
 */
CMP_ID_APP_MAIN_CONTAINER = "WADMINCMS.APP.GUI.CONTAINER";
CMP_ID_APP_USERS_ADDWINDOW = "WADMINCMS.APP.USERS.ADDWINDOW"
CMP_ID_APP_USERS_ADDWINDOW_FORM = "WADMINCMS.APP.USERS.ADDWINDOW.FORM"
CMP_ID_APP_USERS_DETAIL = "WADMINCMS.APP.USERS.DETAIL";
CMP_ID_APP_USERS_LOGINS_LIST_TAB = "WADMINCMS.APP.USERS.LOGINS.LIST.TAB";
CMP_ID_APP_USERS_DETAIL_FORM = "WADMINCMS.APP.USERS.DETAIL.FORM";
CMP_ID_APP_SYSTEM_CONFIGURATIONLIST = "WADMINCMS.APP.SYSTEM.CONFIGURATIONLIST";
CMP_ID_APP_SYSTEM_CONFIGURATIONGROUPADDWINDOW = "WADMINCMS.APP.SYSTEM.CONFIGURATIONGROUPADDWINDOW";
CMP_ID_APP_SYSTEM_CONFIGURATIONGROUPADDWINDOW_FORM = "WADMINCMS.APP.SYSTEM.CONFIGURATIONGROUPADDWINDOW.FORM";
CMP_ID_APP_SYSTEM_CONFIGURATIONITEMDETAILWINDOW = "WADMINCMS.APP.SYSTEM.CONFIGURATIONITEMDETAILWINDOW";
CMP_ID_APP_SYSTEM_CONFIGURATIONITEMDETAILWINDOW_FORM = "WADMINCMS.APP.SYSTEM.CONFIGURATIONITEMDETAILWINDOW.FORM";
CMP_ID_APP_SYSTEM_CONFIGURATIONITEMADDWINDOW = "WADMINCMS.APP.SYSTEM.CONFIGURATIONITEMADDWINDOW";
CMP_ID_APP_SYSTEM_CONFIGURATIONITEMADDWINDOW_FORM = "WADMINCMS.APP.SYSTEM.CONFIGURATIONITEMADDWINDOW.FORM";
CMP_ID_APP_SYSTEM_CONFIGURATION_GROUP_ITEMS_LIST = "WADMINCMS.APP.SYSTEM.CONFIGURATION_GROUP_ITEMS_LIST";
CMP_ID_APP_WEB_PAGE_DETAIL = "WADMINCMS.APP.WEB.PAGE_DETAIL";

/**
 * Application stores
 */
CMP_ID_STORE_USERS_LIST = "WADMINCMS.STORE.USERS.LIST"
CMP_ID_STORE_USERS_LOGINS_LIST = "WADMINCMS.STORE.USERS.LOGINS.LIST";
CMP_ID_STORE_SYSTEM_CONFIGURATIONGROUPSLIST = "WADMINCMS.STORE.SYSTEM.CONFIGURATIONGROUPSLIST";
CMP_ID_STORE_SYSTEM_CONFIGURATIONITEMSLIST = "WADMINCMS.STORE.SYSTEM.CONFIGURATIONITEMSLIST";
CMP_ID_STORE_WEB_PAGES_LIST = "WADMINCMS.STORE.WEB.PAGES_LIST";
CMP_ID_STORE_COMPANY_JOBS_LIST = "WADMINCMS.STORE.COMPANY.JOBS_LIST";
CMP_ID_STORE_COMPANY_JOB_CANDIDATES_LIST = "WADMINCMS.STORE.COMPANY.JOB_CANDIDATES_LIST";

/**
 * Application proxy URL mapping
 */
PROXY_URL_USERS_LIST = "/admin/users/list";
PROXY_URL_USER_ACCOUNT_CREATE = "/admin/users/account-create";
PROXY_URL_USER_ACCOUNT_GET = "/admin/users/get/{0}";
PROXY_URL_USER_ACCOUNT_UPDATE = "/admin/users/update/{0}";
PROXY_URL_USER_ACCOUNT_LOGINS_LIST = "/admin/users/logins-list/{0}";
PROXY_URL_USER_ACCOUNT_DELETE = "/admin/users/delete/{0}";
PROXY_URL_SYSTEM_CONFIGURATIONGROUPSLIST = "/admin/configuration/list-groups";
PROXY_URL_SYSTEM_CONFIGURATIONITEMSLIST = "/admin/configuration/list-items/{0}";
PROXY_URL_SYSTEM_CONFIGURATIONGROUPADD = "/admin/configuration/group-add";
PROXY_URL_SYSTEM_CONFIGURATIONITEM_UPDATE = "/admin/configuration/item-update/{0}";
PROXY_URL_SYSTEM_CONFIGURATIONITEM_ADD = "/admin/configuration/item-add/{0}";
PROXY_URL_SYSTEM_CONFIGURATIONITEM_DELETE = "/admin/configuration/item-delete/{0}";
PROXY_URL_SYSTEM_CONFIGURATIONGROUP_DELETE = "/admin/configuration/group-delete/{0}";
PROXY_URL_WEB_PAGES_LIST = "/admin/web/pages-list";
PROXY_URL_WEB_PAGE_GET = "/admin/web/page-detail/{0}";
PROXY_URL_WEB_PAGE_UPDATE = "/admin/web/page-update/{0}";
PROXY_URL_WEB_PAGE_UPDATE_CONTENT = "/admin/web/page-update-content/{0}";
PROXY_URL_WEB_PAGE_DELETE = "/admin/web/page-delete/{0}";
PROXY_URL_COMPANY_JOBS_LIST = "/admin/company/jobs-list";
PROXY_URL_COMPANY_JOB_CREATE = "/admin/company/job-create";
PROXY_URL_COMPANY_JOB_DETAIL = "/admin/company/job-detail/{0}";
PROXY_URL_COMPANY_JOB_CANDIDATES_LIST = "/admin/company/job-candidates-list/{0}";