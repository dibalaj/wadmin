<?php

namespace App;

use Nette,
	Nette\Application\Routers\RouteList,
	Nette\Application\Routers\Route,
	Nette\Application\Routers\SimpleRouter;


/**
 * Router factory.
 */
class RouterFactory
{

	/**
	 * @return \Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList();

    // AdminModule
    $router[] = new Route('/admin/<presenter>/<action>/[<id>]', [
      'presenter' => 'Main',
      'action'    => 'default',
      'id'        => NULL,
      'module'    => 'Admin'
    ]);

    $router[] = new Route('/stranka-nenalezena-404.html', [
      'presenter' => 'Error',
      'action'    => 'default',
      'id'        => NULL,
      'module'    => 'Front'
    ]);

    // FrontModule
		$router[] = new Route('/<page=o-nas>', [
      'presenter' => 'Page',
      'action'    => 'default',
      'id'        => NULL,
      'module'    => 'Front'
    ]);



		return $router;
	}

}
