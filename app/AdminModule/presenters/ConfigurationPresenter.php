<?
/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 */

namespace App\AdminModule\Presenters;
use Nette;

/**
 * WAdminCMS - AdminModule - Configuration presenter
 *
 * @author      Jakub Dibala
 * @package     WAdminCMS
 */
class ConfigurationPresenter extends Nette\Application\UI\Presenter
{
  /**
   * Groups DB table name
   */
  const DB_TABLE_GROUPS = "configuration_groups";

  /**
   * Keys DB table name
   */
  const DB_TABLE_KEYS = "configuration_items";


  /**
   * API - list groups
   *
   * @return void
   */
  public function renderListGroups()
  {
    /** @var Nette\Database\Context $db */
    $db = $this->context->getByType('Nette\Database\Context');

    // load data
    $data = $db->table(self::DB_TABLE_GROUPS)
      ->where("deleted = 0")
      ->fetchAssoc('id');

    // send JSON
    $this->sendJson([
      "success"   => true,
      "data"      => array_values($data),
    ]);
  }


  /**
   * API - POST - add group
   *
   * Expecting input data:
   * ---------------------
   * - name = group name
   * - description = group description
   *
   * @return void
   */
  public function renderGroupAdd()
  {
    /** @var Nette\Database\Context $db */
    $db = $this->context->getByType('Nette\Database\Context');

    /** @var Nette\Http\Request $httpRequest */
    $httpRequest = $this->context->getByType('Nette\Http\Request');

    // load input data
    $inputName = $httpRequest->getPost("name");
    $inputDesc = $httpRequest->getPost("description");

    // input validation
    if (!$inputName || !$inputDesc)
    {
      $this->sendJson([
        "success" => false,
        "error"   => "INVALID_INPUT_DATA"
      ]);
    }

    // check, if group doesnt exists
    if ($db->table(self::DB_TABLE_GROUPS)->where("name = ?", $inputName)->where("deleted = 0")->fetch())
    {
      $this->sendJson([
        "success" => false,
        "error"   => "DUPLICIT_RECORD_FOUND"
      ]);
    }

    // insert data
    $insertData = [
      "name"          => $inputName,
      "description"   => $inputDesc,
      "created_date"  => date('U'),
    ];

    // finally, creates group record
    if (!$db->table(self::DB_TABLE_GROUPS)->insert($insertData))
    {
      $this->sendJson([
        "success" => false,
        "error"   => "CREATE_RECORD_FAILED"
      ]);
    }

    // everything is OK
    $this->sendJson([
      "success" => true,
    ]);
  }


  /**
   * API - get items (keys) list
   *
   * @param integer $id group id
   * @return void
   */
  public function renderListItems($id)
  {
    /** @var Nette\Database\Context $db */
    $db = $this->context->getByType('Nette\Database\Context');

    // load data
    $data = $db->table(self::DB_TABLE_KEYS)
      ->where("deleted = 0")
      ->where('group_id = ?', $id)
      ->order('id ASC')
      ->fetchAssoc('id');

    $this->sendJson([
      "success"   => true,
      "data"      => array_values($data),
    ]);
  }


  /**
   * API - add new item (key) to group
   *
   * Expecting input data:
   * ---------------------
   * - key = key name
   * - value = configuration value
   * - description = group description
   *
   * @param integer $id group ID
   * @return void
   */
  public function renderItemAdd($id)
  {
    /** @var Nette\Database\Context $db */
    $db = $this->context->getByType('Nette\Database\Context');

    /** @var Nette\Http\Request $httpRequest */
    $httpRequest = $this->context->getByType('Nette\Http\Request');

    // load input data
    $inputKey = $httpRequest->getPost("key");
    $inputValue = $httpRequest->getPost("value");
    $inputDesc = $httpRequest->getPost("description");

    // input data validation
    if (!$inputKey || !$inputValue || !$inputDesc)
    {
      $this->sendJson([
        "success" => false,
        "error"   => "INVALID_INPUT_DATA",
      ]);
    }

    // check if row with same key does not exists
    if ($db->table(self::DB_TABLE_KEYS)->where("key = ?", $inputKey)->where("deleted = 0")->fetch())
    {
      $this->sendJson([
        "success" => false,
        "error"   => "DUPLICIT_RECORD_FOUND",
      ]);
    }

    // create insert data
    $insertData = [
      "key"           => $inputKey,
      "value"         => $inputValue,
      "description"   => $inputDesc,
      "group_id"      => $id,
      "created_date"  => date('U'),
    ];

    // save data
    if (!$db->table('configuration_items')->insert($insertData))
    {
      $this->sendJson([
        "success" => false,
        "error"   => "CREATE_RECORD_FAILED",
      ]);
    }

    // OK
    $this->sendJson([
      "success" => true,
    ]);
  }


  /**
   * API - update key data
   *
   * @param integer $id item id
   * @return void
   */
  public function renderItemUpdate($id)
  {
    /** @var Nette\Database\Context $db */
    $db = $this->context->getByType('Nette\Database\Context');

    /** @var Nette\Http\Request $httpRequest */
    $httpRequest = $this->context->getByType('Nette\Http\Request');

    // load input data
    $inputKey = $httpRequest->getPost('key');
    $inputValue = $httpRequest->getPost('value');
    $inputDescription = $httpRequest->getPost('description');

    // load saved data
    $item = $db->table(self::DB_TABLE_KEYS)->get($id);

    if (!$item)
    {
      $this->sendJson([
        "success" => false,
        "error"   => "RECORD_NOT_FOUND",
      ]);
    }

    // load existing record from DB
    $record = $db->table(self::DB_TABLE_KEYS)
        ->where('group_id = ?', $item->group_id)
        ->where('key = ?', $inputKey)
        ->fetch();

    // check if key is not duplicit
    if ($record && $record->id != $id)
    {
      $this->sendJson([
        "success" => false,
        "error"   => "DUPLICIT_RECORD_FOUND",
      ]);
    }

    // create update data
    $updateData = [
      "key"         => $inputKey,
      "value"       => $inputValue,
      "description" => $inputDescription,
    ];

    // update data in DB
    if (!$db->table(self::DB_TABLE_KEYS)->where('id = ?', $id)->update($updateData))
    {
      $this->sendJson([
        "success" => false,
        "error"   => "UPDATE_FAILED",
      ]);
    }

    $this->sendJson([
      "success" => true,
    ]);
  }


  /**
   * API - key delete by ID
   *
   * @param integer $id key ID
   * @return void
   */
  public function renderItemDelete($id)
  {
    /** @var Nette\Database\Context $db */
    $db = $this->context->getByType('Nette\Database\Context');

    // delete key
    $del = $db->table(self::DB_TABLE_KEYS)->where('id = ?', $id)->update([
      "deleted"       => 1,
    ]);

    $this->sendJson([
      "success" => (bool)$del
    ]);
  }


  /**
   * API - delete group by ID
   *
   * @param integer $id group ID
   * @return void
   */
  public function renderGroupDelete($id)
  {
    /** @var Nette\Database\Context $db */
    $db = $this->context->getByType('Nette\Database\Context');

    // delete group
    if (!$db->table(self::DB_TABLE_GROUPS)->where('id = ?', $id)->update(["deleted"  => 1]))
      $this->sendJson(["success" => false]);

    // then, delete all group keys
    $db->table('configuration_items')->where('group_id = ?', $id)->update(["deleted"  => 1]);

    // OK
    $this->sendJson([
      "success" => true
    ]);
  }
}