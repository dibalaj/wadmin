<?
/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 */

namespace App\AdminModule\Presenters;
use Nette;

/**
 * WAdminCMS - AdminModule - Web presenter
 *
 * @author      Jakub Dibala
 * @package     WAdminCMS
 */
class WebPresenter extends Nette\Application\UI\Presenter
{
  /**
   * DB table - pages
   */
  const DB_TABLE_PAGES = "pages";


  /**
   * API - get pages list
   *
   * @return void
   */
  public function renderPagesList()
  {
    /** @var Nette\Database\Context $db */
    $db = $this->context->getByType('Nette\Database\Context');

    // load data
    $data = $db->table(self::DB_TABLE_PAGES)
      ->where("deleted = 0")
      ->fetchAssoc('id');

    // send JSON
    $this->sendJson([
      "success" => true,
      "data"    => array_values($data),
    ]);
  }


  /**
   * API - page update
   *
   * @param integer $id page ID
   * @return void
   */
  public function renderPageUpdate($id)
  {
    /** @var Nette\Database\Context $db */
    $db = $this->context->getByType('Nette\Database\Context');

    /** @var Nette\Http\Request $httpRequest */
    $httpRequest = $this->context->getByType('Nette\Http\Request');

    // read input
    $inputTitle = $httpRequest->getPost('title');
    $inputUrl = $httpRequest->getPost('url');
    $inputPublished = $httpRequest->getPost('published');

    // validation
    if (!$inputTitle || !$inputUrl)
    {
      $this->sendJson([
        "success" => false,
        "error"   => "INVALID_INPUT_DATA",
      ]);
    }

    // create update data array
    $updateData = [
      "title"     => $inputTitle,
      "url"       => $inputUrl,
      "published" => (bool)$inputPublished,
    ];

    // save changes
    $db->table(self::DB_TABLE_PAGES)->where('id = ?', $id)->update($updateData);

    // OK
    $this->sendJson([
      "success" => true,
    ]);
  }


  /**
   * API - page update content
   *
   * @param integer $id page ID
   * @return void
   */
  public function renderPageUpdateContent($id)
  {
    /** @var Nette\Database\Context $db */
    $db = $this->context->getByType('Nette\Database\Context');

    /** @var Nette\Http\Request $httpRequest */
    $httpRequest = $this->context->getByType('Nette\Http\Request');

    // read input
    $content = $httpRequest->getPost('content');

    // create update data array
    $updateData = [
      "content" => $content,
    ];

    // save changes
    $db->table(self::DB_TABLE_PAGES)->where('id = ?', $id)->update($updateData);

    // rewrite page template
    $tpl = "{block content}".$content."{/block}";

    $templateFile = $this->context->getParameters()['appDir'].'/FrontModule/templates/'.$id.'.latte';
    file_put_contents($templateFile, $tpl);

    // @TODO: flush frontmodule cache ...

    // OK
    $this->sendJson([
      "success" => true,
    ]);
  }


  /**
   * API - page details
   *
   * @param integer $id page ID
   * @return void
   */
  public function renderPageDetail($id)
  {
    /** @var Nette\Database\Context $db */
    $db = $this->context->getByType('Nette\Database\Context');

    // load data from DB
    /** @var Nette\Database\Table\ActiveRow $data */
    $data = $db->table(self::DB_TABLE_PAGES)->get($id);

    // send back to frontend
    $this->sendJson([
      "success" => true,
      "data"    => $data->toArray(),
    ]);
  }
}