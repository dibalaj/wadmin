<?
/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 */
namespace App\AdminModule\Presenters;
use Nette;

/**
 * WAdminCMS - AdminModule - Company presenter
 *
 * @author      Jakub Dibala
 * @package     WAdminCMS
 */
class CompanyPresenter extends Nette\Application\UI\Presenter
{
  /**
   * DB table - pages
   */
  const DB_TABLE_JOBS = "jobs2";


  /**
   * API - get pages list
   *
   * @return void
   */
  public function renderJobsList()
  {
    /** @var Nette\Database\Context $db */
    $db = $this->context->getByType('Nette\Database\Context');

    // load data
    $data = $db->table(self::DB_TABLE_JOBS)
      ->where("deleted = 0")
      ->fetchAssoc('id');

    foreach ($data as $key => $row)
    {
      $data[$key]['date_from'] = Nette\Utils\DateTime::from($row['date_from'])->format('d.m.Y');
    }

    // send JSON
    $this->sendJson([
      "success" => true,
      "data"    => array_values($data),
    ]);
  }


  /**
   * API - create job
   *
   * @return void
   */
  public function renderJobCreate()
  {
    /** @var Nette\Http\Request $httpRequest */
    $httpRequest = $this->context->getByType('Nette\Http\Request');

    print_r($httpRequest->getPost()); exit;

    // load input POST data
    $inputTitle = $httpRequest->getPost('description');
    $inputReward = $httpRequest->getPost('reward');
    $inputDateFrom = $httpRequest->getPost('date_from');
    $inputDateTo = $httpRequest->getPost('date_to');
    $inputTimeFrom = $httpRequest->getPost('time_from');
    $inputTimeTo = $httpRequest->getPost('time_to');
    $inputPlaceAddress = $httpRequest->getPost('place_address');
    $inputPlaceCity = $httpRequest->getPost('place_city');
    $inputPlaceZipCode = $httpRequest->getPost('place_zip_code');
    $inputInfo = $httpRequest->getPost('info');
    $inputAuthorName = $httpRequest->getPost('author_name');
    $inputAuthorEmail = $httpRequest->getPost('author_email');
    $inputAuthorPhone = $httpRequest->getPost('author_phone');

    // form validation
    if (!$inputTitle || !$inputReward || !$inputDateFrom || !$inputPlaceAddress || !$inputPlaceCity || !$inputPlaceZipCode || !$inputAuthorName || !$inputAuthorEmail || !$inputAuthorPhone)
    {
      $this->sendJson([
        "success" => false,
        "error"   => "INVALID_INPUT_DATA",
      ]);
    }

    /** @var Nette\Database\Context $db */
    $db = $this->context->getByType('Nette\Database\Context');

    // create insert data
    $insertData = [
      "title"           => $inputTitle,
      "reward"          => $inputReward,
      "date_from"       => Nette\Utils\DateTime::from($inputDateFrom)->format('Y-m-d'),
      "date_to"         => Nette\Utils\DateTime::from($inputDateTo)->format('Y-m-d'),
      "time_from"       => $inputTimeFrom,
      "time_to"         => $inputTimeTo,
      "place_address"   => $inputPlaceAddress,
      "place_city"      => $inputPlaceCity,
      "place_zip_code"  => $inputPlaceZipCode,
      "info"            => $inputInfo,
      "author_name"     => $inputAuthorName,
      "author_email"    => $inputAuthorEmail,
      "author_phone"    => $inputAuthorPhone,
      "created_date"    => date('U')
    ];

    // add to database
    $db->table(self::DB_TABLE_JOBS)->insert($insertData);

    // OK
    $this->sendJson([
      "success" => true,
    ]);
  }


  public function renderJobDetail($id)
  {
    /** @var Nette\Database\Context $db */
    $db = $this->context->getByType('Nette\Database\Context');

    $job = $db->table(self::DB_TABLE_JOBS)->get($id);

    if (!$job)
    {
      $this->sendJson([
        "success" => false,
        "error"   => "RECORD_NOT_FOUND",
      ]);
    }

    $job = $job->toArray();

    $job['date_from'] = Nette\Utils\DateTime::from($job['date_from'])->format('d.m.Y');
    $job['date_to'] = Nette\Utils\DateTime::from($job['date_to'])->format('d.m.Y');

    $this->sendJson([
      "success"   => true,
      "data"      => $job
    ]);
  }


  public function renderJobCandidatesList($id)
  {
    /** @var Nette\Database\Context $db */
    $db = $this->context->getByType('Nette\Database\Context');

    // load data
    $data = $db->table('jobs_candidates')
      ->where("deleted = 0")
      ->where("offer_id = ?", $id)
      ->fetchAssoc('id');

    foreach ($data as $key => $row)
    {
      $data[$key]['created_date'] = Nette\Utils\DateTime::from($row['created_date'])->format('d.m.Y H:i:s');
      $data[$key]['birth_date'] = Nette\Utils\DateTime::from($row['birth_date'])->format('d.m.Y');
    }

    // send JSON
    $this->sendJson([
      "success" => true,
      "data"    => array_values($data),
    ]);
  }
}