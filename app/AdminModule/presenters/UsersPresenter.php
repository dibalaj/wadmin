<?
namespace App\AdminModule\Presenters;
use Nette;

class UsersPresenter extends Nette\Application\UI\Presenter
{
  /**
   *
   */
  public function renderList()
  {
    /** @var Nette\Database\Context $db */
    $db = $this->context->getByType('Nette\Database\Context');
    $data = $db->table("users")->where("deleted = 0")->fetchAssoc('id');

    $users = [];

    foreach ($data as $user)
    {
      $users[] = $user;
    }

    $this->sendJson([
      "success"   => true,
      "data"     => $users,
    ]);
  }


  public function renderAccountCreate()
  {
    // TODO - validace

    /** @var Nette\Http\Request $httpRequest */
    $httpRequest = $this->context->getByType('Nette\Http\Request');
    $postData = $httpRequest->getPost();

    $insertData = [
      "username"      => $postData["username"],
      "name"          => $postData["name"],
      "surname"       => $postData["surname"],
      "email"         => $postData["email"],
      "created_date"  => date('U')
    ];

    /** @var Nette\Database\Context $db */
    $db = $this->context->getByType('Nette\Database\Context');
    $ins = $db->table('users')->insert($insertData);

    $this->sendJson([
      "success"   => (bool)$ins,
    ]);
  }


  public function renderGet($id)
  {
    $db = $this->context->getByType('Nette\Database\Context');

    $user = $db->table('users')->get($id);

    $this->sendJson([
      "success"   => true,
      "data"  => $user->toArray()
    ]);
  }


  public function renderUpdate($id)
  {
    $db = $this->context->getByType('Nette\Database\Context');

    // data ze vstupu
    $postData = $this->context->getByType('Nette\Http\Request')->getPost();

    $db->table('users')->get($id)->update($postData);

    $this->sendJson([
      "success" => true,
    ]);

    dump($postData); exit;
  }


  public function renderLoginsList($id)
  {
    /** @var Nette\Database\Context $db */
    $db = $this->context->getByType('Nette\Database\Context');
    $data = $db->table("users_logins")->where("deleted = 0")->where('user_id = ?', $id)->fetchAssoc('id');

    $logins = [];
    foreach ($data as $login)
    {
      $l = $login;
      $l['created_date'] = Nette\Utils\DateTime::from($l['created_date'])->format('d.m.Y H:i:s');
      $logins[] = $l;
    }

    $this->sendJson([
      "success"   => true,
      "data"     => $logins,
    ]);
  }


  public function renderDelete($id)
  {
    /** @var Nette\Database\Context $db */
    $db = $this->context->getByType('Nette\Database\Context');

    // delete
    $del = $db->table('users')->where('id = ?', $id)->update([
      "deleted"       => 1,
      "deleted_date"  => date('U')
    ]);

    $this->sendJson([
      "success" => (bool)$del
    ]);
  }
}