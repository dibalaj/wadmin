<?php

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

$configurator->setDebugMode(true);
$configurator->enableDebugger(__DIR__ . '/../log');

Tracy\Debugger::$maxDepth = 10;
Tracy\Debugger::$maxLen = 500;


$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
	->addDirectory(__DIR__)
  ->addDirectory(dirname(__DIR__).'/vendor/wadminlibs/src')
	->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.local.neon');

WAdminLibs\Core\ErrorMailer::register();

$container = $configurator->createContainer();

return $container;
