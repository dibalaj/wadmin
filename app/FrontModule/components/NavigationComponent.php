<?
/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 */
namespace App\FrontModule\Components;
use Nette;

/**
 * Navigation component
 *
 * @author      Jakub Dibala
 * @package     App\FrontModule\Components
 */
class NavigationComponent extends Nette\Application\UI\Control
{
  /**
   * Main render method
   *
   * @return void
   */
  public function render()
  {
    // set template file
    $this->template->setFile(dirname(__FILE__).'/templates/NavigationComponent.latte');

    // set navigation nodes to template
    $this->template->nodes = $this->getNavigation();

    // render the template
    $this->template->render();
  }


  /**
   * Returns navigation nodes array
   *
   * @return array
   */
  private function getNavigation()
  {
    /** @var Nette\Database\Context $db */
    $db = $this->presenter->context->getByType('Nette\Database\Context');

    $ret = $db->query("
      SELECT navigation.id, pages.title as title, pages.url as url, false as children, navigation.parent_id
      FROM navigation
      INNER JOIN pages ON navigation.page_id=pages.id
      WHERE navigation.deleted=0
      AND pages.deleted=0
      AND pages.published=1
    ")->fetchAssoc('id');

    foreach ($ret as $key => $node)
    {
      if ($node['parent_id'])
      {
        if (!is_array($ret[$node['parent_id']]['children']))
          $ret[$node['parent_id']]['children'] = [];

        $ret[$node['parent_id']]['children'][] = $node;
        unset($ret[$key]);
      }
    }

    return $ret;
  }
}