<?
namespace App\FrontModule\Components;
use Nette;

/**
 * List component
 *
 * @package App\FrontModule\Components
 */
class JobsListComponent extends Nette\Application\UI\Control
{
  public function render()
  {
    // set template file
    $this->template->setFile(dirname(__FILE__).'/templates/JobsListComponent.latte');

    /** @var Nette\Database\Context $db */
    $db = $this->presenter->context->getByType('Nette\Database\Context');

    $this->template->jobs = $db->table('jobs2')->where('deleted = 0')->order('date_from ASC')->fetchAll();

    // render the template
    $this->template->render();
  }


  public function handleFindJob($id)
  {
    $job = $this->presenter->context->getByType('Nette\Database\Context')->table('jobs2')->get($id);

    if (!$job || $job->deleted)
    {
      echo "<p style='padding: 20px 30px;'>Je nám líto, ale tato pracovní nabídka nebyla nalezena!</p>";
      $this->presenter->terminate();
    }

    $this->template->setFile(dirname(__FILE__).'/templates/JobsListComponent_Detail.latte');
    $this->template->job = $job;
    $this->template->googleMapsAddress = $job->place_address.', '.$job->place_city.' '.$job->place_zip_code;
    $this->template->render();
    $this->presenter->terminate();
  }
}