<?
/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 */
namespace App\FrontModule\Presenters;
use App\AdminModule\Presenters\WebPresenter, Nette;

/**
 * Main page presenter for displaing page content
 *
 * @author      Jakub Dibala
 * @package     App\FrontModule\Presenters
 */
class PagePresenter extends Nette\Application\UI\Presenter
{
  /**
   * RENDER - default view
   *
   * @return void
   */
  public function renderDefault()
  {
    /** @var Nette\Http\Context $db */
    $db = $this->context->getByType('Nette\Database\Context');

    // load page record from DB
    $page = $db->table(WebPresenter::DB_TABLE_PAGES)->where("url = ?", $this->getParameter('page'))->fetch();

    // no page found?
    if (!$page)
    {
      $this->redirect('Error:default');
      return;
    }

    $templateFile = $this->context->getParameters()['appDir'].'/FrontModule/templates/'.$page->id.'.latte';
    $this->template->setFile($templateFile);

    // page settings
    $this->template->pageTitle = $page->title;
    $this->template->metaDescription = $page->meta_description;
    $this->template->metaKeywords = $page->meta_keywords;
  }


  public function createComponent($name)
  {
    $serviceName = 'control'.ucfirst($name);

    $cmp = $this->context->getService($serviceName);

    return $cmp;
  }
}