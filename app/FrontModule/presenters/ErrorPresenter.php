<?
/**
 * -----------------------------------------------------------------------
 * This file is part of WAdmin CMS.
 *
 * WAdmin CMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WAdmin CMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WAdmin CMS.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------
 */
namespace App\FrontModule\Presenters;
use Nette;

/**
 * Main page presenter for displaing page content
 *
 * @author      Jakub Dibala
 * @package     App\FrontModule\Presenters
 */
class ErrorPresenter extends Nette\Application\UI\Presenter
{
  /**
   * RENDER - default view
   *
   * @return void
   */
  public function renderDefault()
  {
    $this->template->metaDescription = 'popis';
    $this->template->metaKeywords = 'slovo';
    $this->template->pageTitle = 'Stránka nenalezena';
  }


  public function createComponentNavigation()
  {
    return $this->context->getService('controlNavigation');
  }
}